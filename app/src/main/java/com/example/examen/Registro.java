package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registro extends AppCompatActivity {

    EditText usuario, password, repassword;
    Button btnregistro, btnlogin;
    SQLite DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        usuario = (EditText) findViewById(R.id.usuario);
        password = (EditText) findViewById(R.id.password);
        repassword = (EditText) findViewById(R.id.repassword);
        btnregistro = (Button) findViewById(R.id.btnregistro);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        DB = new SQLite(this);

        btnregistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = usuario.getText().toString();
                String pass = password.getText().toString();
                String repass = repassword.getText().toString();

                if(user.equals("")||pass.equals("")||repass.equals(""))
                    Toast.makeText(Registro.this, "Por favor rellene todos los campos", Toast.LENGTH_SHORT).show();
                else {
                    if(pass.equals(repass)){
                        boolean revisar = DB.revisarusuario(user);
                        if (revisar==false){
                            boolean insertar = DB.insertData(user,pass);
                            if (insertar==true){
                                Toast.makeText(Registro.this, "Registro exitoso", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(Registro.this, "Registro fallido", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            Toast.makeText(Registro.this, "¡El usuario ya existe! Inicia sesión", Toast.LENGTH_SHORT).show();
                        }
                    } else{
                        Toast.makeText(Registro.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });
    }
}