package com.example.examen;

public class Grupos {
    private String id;
    private String ordenamiento;
    private String direccion;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrdenamiento() {
        return ordenamiento;
    }

    public void setOrdenamiento(String ordenamiento) {
        this.ordenamiento = ordenamiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
