package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class pdf extends AppCompatActivity {

    Button crearpdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        crearpdf = (Button) findViewById(R.id.crearpdf);
        try {
            createpdf();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createpdf() throws FileNotFoundException {
        crearpdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pdfpath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
                File file = new File(pdfpath, "PDF.pdf");
                try {
                    OutputStream outputStream = new FileOutputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                PdfWriter writer = null;
                try {
                    writer = new PdfWriter(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                PdfDocument pdfDocument = new PdfDocument(writer);
                Document document = new Document(pdfDocument);

                Paragraph paragraph = new Paragraph("Examen PGM");
                Paragraph paragraph1 = new Paragraph("Este examen fue realizado por Pablo Ontiveros");
                Paragraph paragraph2 = new Paragraph("Android Studio es el entorno de desarrollo integrado (IDE) oficial para el desarrollo de apps para\n " +
                        "Android y está basado en IntelliJ IDEA. Además del potente editor de códigos y las herramientas para desarrolladores de IntelliJ,\n" +
                        " Android Studio ofrece incluso más funciones que aumentan tu productividad cuando desarrollas apps para Android, como las siguientes:\n" +
                        "\n" +
                        "Un sistema de compilación flexible basado en Gradle\n" +
                        "Un emulador rápido y cargado de funciones\n" +
                        "Un entorno unificado donde puedes desarrollar para todos los dispositivos Android\n" +
                        "Aplicación de cambios para insertar cambios de código y recursos a la app en ejecución sin reiniciarla\n" +
                        "Integración con GitHub y plantillas de código para ayudarte a compilar funciones de apps comunes y también importar código de muestra\n" +
                        "Variedad de marcos de trabajo y herramientas de prueba\n" +
                        "Herramientas de Lint para identificar problemas de rendimiento, usabilidad y compatibilidad de versiones, entre otros\n" +
                        "Compatibilidad con C++ y NDK\n" +
                        "Compatibilidad integrada con Google Cloud Platform, que facilita la integración con Google Cloud Messaging y App Engine");
                document.add(paragraph);
                document.add(paragraph1);
                document.add(paragraph2);
                document.close();
                Toast toast = Toast.makeText(getApplicationContext(), "PDF Creado", Toast.LENGTH_SHORT);
                toast.show();
            }

        });

    }
}