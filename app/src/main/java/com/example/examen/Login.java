package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText usuario, password;
    Button btnlogin, btnregistro;
    SQLite DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //TODO no es necesario (widget)
        usuario = (EditText) findViewById(R.id.usuario1);
        password = (EditText) findViewById(R.id.password1);
        btnlogin = (Button) findViewById(R.id.btnlogin1);
        btnregistro = (Button) findViewById(R.id.btnregistro1);
        DB = new SQLite(this);

            btnlogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String user = usuario.getText().toString();
                    String pass = password.getText().toString();


                    if (user.equals("") || pass.equals(""))
                        Toast.makeText(Login.this, "Por favor rellene todos los campos", Toast.LENGTH_SHORT).show();
                    else {
                        boolean revisaruserpass = DB.revisarusuariopassword(user, pass);
                        //TODO lo podrias simpplificar a: if (revisaruserpass) {
                        if (revisaruserpass == true) {
                            Toast.makeText(Login.this, "Inicio de sesión exitoso", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(Login.this, "Usuario invalido", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            btnregistro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), Registro.class);
                    startActivity(intent);
                }
            });
        }
    }